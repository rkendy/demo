# README

Aplicação Spring api de Cliente.

### Requisitos

- Java 11
- Docker

### Instruções para levantar o ambiente

`$ mvn clean install -DskipTests`

`$ docker-compose down --rmi all`

`$ docker-compose up`

### Rodar testes com mysql em execução

`$ mvn clean test`

### Rodar testes no Postman

- Importar arquivo "Cliente.postman_collection.json"
- Executar a coleção "Collection Cliente"
