package com.example.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import com.example.demo.model.Cliente;
import com.example.demo.repository.ClienteRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClienteControllerTestIT {
    HttpHeaders headers = new HttpHeaders();
    TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    ClienteRepository repository;

    @LocalServerPort
    int port;

    @Test
    public void givenValidId_whenGet_thenReturnSuccess() throws Exception {

        Cliente novo = Cliente.builder().cpf("001").nome("Nome XYZ").dataNascimento(LocalDate.parse(("2000-01-01")))
                .build();

        novo = repository.save(novo);

        ResponseEntity<String> response = makeGetRequest(novo.getId());

        assertEquals(HttpStatus.OK.value(), response.getStatusCode().value());
    }

    protected ResponseEntity<String> makeGetRequest(Long id) {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        return restTemplate.exchange(createURLWithPort(ClienteController.ENDPOINT + "/" + id), HttpMethod.GET, entity,
                String.class);
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
