package com.example.demo.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Optional;

import com.example.demo.assembler.ClienteModelAssembler;
import com.example.demo.dto.ClienteDto;
import com.example.demo.exception.ExceptionAdvice;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Cliente;
import com.example.demo.repository.ClienteRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class ClienteControllerTest {
    @Mock
    private ClienteRepository repository;

    @InjectMocks
    ClienteController controller;

    @Mock
    ClienteModelAssembler assembler;

    @Mock
    PagedResourcesAssembler<Cliente> pageAssembler;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller) //
                .setControllerAdvice(new ExceptionAdvice()) //
                .build();
    }

    @Test
    void givenValidId_whenFindById_thenReturnClient() throws Exception {

        Cliente cliente = createCliente("0212223123", "Nome cliente", LocalDate.now());

        when(repository.findById(1L)).thenReturn(Optional.of(cliente));
        when(assembler.toModel(any())).thenReturn(toDto(cliente));

        mockMvc.perform(get(ClienteController.ENDPOINT + "/1")) //
                .andExpect(status().isOk()) //
                .andExpect(jsonPath("$.nome").value((cliente.getNome()))) //
                .andExpect(jsonPath("$.cpf").value(cliente.getCpf()));
    }

    @Test
    void givenValidId_whenFindById_thenReturnCorrectIdade() throws Exception {
        LocalDate dataNascimento = LocalDate.parse("2001-11-09");

        Cliente cliente = createCliente("0212223123", "Nome cliente", dataNascimento);

        when(repository.findById(1L)).thenReturn(Optional.of(cliente));
        when(assembler.toModel(any())).thenReturn(toDto(cliente));

        mockMvc.perform(get(ClienteController.ENDPOINT + "/1")) //
                .andExpect(status().isOk()) //
                .andExpect(jsonPath("$.nome").value((cliente.getNome()))) //
                .andExpect(jsonPath("$.cpf").value(cliente.getCpf())); //
    }

    @Test
    void givenInvalidId_whenFindById_thenReturnNotFound() throws Exception {
        when(repository.findById(anyLong())).thenThrow(NotFoundException.class);

        mockMvc.perform(get(ClienteController.ENDPOINT + "/1")) //
                .andExpect(status().isNotFound());
    }

    @Test
    void givenValidCliente_whenCreate_thenReturnSuccess() throws Exception {
        Cliente cliente = createCliente("0212223123", "Nome cliente", LocalDate.now());
        when(repository.save(any(Cliente.class))).thenReturn(cliente);
        when(assembler.toModel(any())).thenReturn(toDto(cliente));

        mockMvc.perform(post(ClienteController.ENDPOINT) //
                .contentType(MediaType.APPLICATION_JSON) //
                .content(toJson(toDto(cliente)))) //
                .andExpect(status().isCreated()) //
                .andExpect(jsonPath("$.nome").value((cliente.getNome()))) //
                .andExpect(jsonPath("$.cpf").value(cliente.getCpf()));
    }

    @Test
    void givenValidCliente_whenUpdate_thenSuccess() throws Exception {
        Cliente cliente = createCliente("0212223123", "Nome cliente", LocalDate.now());

        when(repository.findById(anyLong())).thenReturn(Optional.of(cliente));
        when(repository.save(any())).thenReturn(cliente);
        when(assembler.toModel(any())).thenReturn(toDto(cliente));

        mockMvc.perform( //
                put(ClienteController.ENDPOINT + "/1") //
                        .contentType(MediaType.APPLICATION_JSON) //
                        .content(toJson(toDto(cliente)))) //
                .andExpect(status().isOk()) //
                .andExpect(jsonPath("$.nome").value((cliente.getNome()))) //
                .andExpect(jsonPath("$.cpf").value((cliente.getCpf())));
    }

    @Test
    void givenValidCliente_whenPatch_thenSuccess() throws Exception {
        Cliente cliente = createCliente("0212223123", "Nome cliente", LocalDate.now());

        when(repository.findById(anyLong())).thenReturn(Optional.of(cliente));
        when(repository.save(any())).thenReturn(cliente);
        when(assembler.toModel(any())).thenReturn(toDto(cliente));

        mockMvc.perform( //
                patch(ClienteController.ENDPOINT + "/1") //
                        .contentType(MediaType.APPLICATION_JSON) //
                        .content(toJson(toDto(cliente)))) //
                .andExpect(status().isOk()) //
                .andExpect(jsonPath("$.nome").value((cliente.getNome())))
                .andExpect(jsonPath("$.cpf").value(cliente.getCpf()));
    }

    @Test
    void givenId_whenDelete_thenSuccess() throws Exception {
        Cliente cliente = createCliente("0212223123", "Nome cliente", LocalDate.now());
        when(repository.findById(anyLong())).thenReturn(Optional.of(cliente));
        doNothing().when(repository).deleteById(anyLong());

        mockMvc.perform( //
                delete(ClienteController.ENDPOINT + "/1")) //
                .andExpect(status().isOk()); //
    }

    private Cliente createCliente(String cpf, String nome, LocalDate nascimento) {
        return Cliente.builder().cpf(cpf).nome(nome).dataNascimento(nascimento).build();
    }

    private ClienteDto toDto(Cliente cliente) {
        return new ModelMapper().map(cliente, ClienteDto.class);
    }

    private String toJson(ClienteDto dto) {
        StringBuilder stb = new StringBuilder();
        if (dto.getCpf() != null) {
            stb.append("\"cpf\": " + "\"" + dto.getCpf() + "\"");
        }
        if (dto.getNome() != null) {
            stb.append("\"nome\": " + "\"" + dto.getNome() + "\"");
        }
        return "{" + stb.toString().replace("\"\"", "\",\"") + "}";
    }
}
