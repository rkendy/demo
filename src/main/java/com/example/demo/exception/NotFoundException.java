package com.example.demo.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(final Class<?> clazz, final Long id) {
        super("NotFoundException with " + clazz.getSimpleName() + " and id " + id);
    }

}