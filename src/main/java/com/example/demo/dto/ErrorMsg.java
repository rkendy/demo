package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ErrorMsg {
    public final String msg;
    public final LocalDateTime timestamp;

    public ErrorMsg(String msg) {
        this.msg = msg;
        this.timestamp = LocalDateTime.now();

    }
}