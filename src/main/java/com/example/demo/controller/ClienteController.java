package com.example.demo.controller;

import java.time.LocalDate;
import java.util.Map;

import com.example.demo.assembler.ClienteModelAssembler;
import com.example.demo.dto.ClienteDto;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Cliente;
import com.example.demo.repository.ClienteRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ClienteController.ENDPOINT)
public class ClienteController {

    public static final String ENDPOINT = "/cliente";

    private ClienteRepository repository;
    private ClienteModelAssembler clienteModelAssember;
    private PagedResourcesAssembler<Cliente> pagedResourcesAssembler;

    private ModelMapper modelMapper;

    @Autowired
    public ClienteController(ClienteRepository repository, ClienteModelAssembler clienteModelAssember,
            PagedResourcesAssembler<Cliente> pagedResourcesAssembler) {
        this.repository = repository;
        this.clienteModelAssember = clienteModelAssember;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
        modelMapper = new ModelMapper();
    }

    @GetMapping
    public ResponseEntity<PagedModel<ClienteDto>> findClientes( //
            @RequestParam(defaultValue = "") String cpf, //
            @RequestParam(defaultValue = "") String nome, //
            @RequestParam(defaultValue = "0") Integer pageNumber, //
            @RequestParam(defaultValue = "10") Integer pageSize) {

        Pageable paging = PageRequest.of(pageNumber, pageSize);
        Page<Cliente> result;
        if ("".equals(cpf) && "".equals(nome)) {
            result = repository.findAll(paging);
        } else {
            result = repository.findByCpfOrNome(cpf, nome, paging);
        }
        PagedModel<ClienteDto> clientes = pagedResourcesAssembler.toModel( //
                result, //
                clienteModelAssember);

        return new ResponseEntity<>(clientes, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClienteDto> getById(@PathVariable Long id) {

        return repository.findById(id) //
                .map(clienteModelAssember::toModel) //
                .map(ResponseEntity::ok) //
                .orElseThrow(() -> new NotFoundException(Cliente.class, id));
    }

    @PostMapping
    public ResponseEntity<ClienteDto> create(@RequestBody ClienteDto clienteDto) {
        Cliente novo = repository.save(convertToModel(clienteDto));
        return ResponseEntity.status(HttpStatus.CREATED).body(clienteModelAssember.toModel(novo));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClienteDto> update(@PathVariable Long id, @RequestBody ClienteDto dto) {

        repository.findById(id).orElseThrow(() -> new NotFoundException(Cliente.class, id));
        Cliente toUpdated = convertToModel(dto);
        toUpdated.setId(id);
        Cliente updatedCliente = repository.save(toUpdated);
        return ResponseEntity.ok(clienteModelAssember.toModel(updatedCliente));
    }

    @PatchMapping("/{id}")
    ResponseEntity<?> patch(@PathVariable Long id, @RequestBody Map<String, Object> clienteMap) {
        Cliente cliente = repository.findById(id).orElseThrow(() -> new NotFoundException(Cliente.class, id));
        cliente = patchCliente(cliente, clienteMap);

        Cliente updated = repository.save(cliente);
        return ResponseEntity.ok(clienteModelAssember.toModel(updated));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        repository.findById(id).orElseThrow(() -> new NotFoundException(Cliente.class, id));
        repository.deleteById(id);
    }

    private Cliente patchCliente(Cliente cliente, Map<String, Object> map) {
        if (map.get("nome") != null)
            cliente.setNome((String) map.get("nome"));
        if (map.get("cpf") != null)
            cliente.setCpf((String) map.get("cpf"));
        if (map.get("dataNascimento") != null)
            cliente.setDataNascimento((LocalDate) map.get("dataNascimento"));
        return cliente;
    }

    private Cliente convertToModel(ClienteDto clienteDto) {
        return modelMapper.map(clienteDto, Cliente.class);
    }

}
