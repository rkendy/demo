package com.example.demo.repository;

import com.example.demo.model.Cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> {
    Page<Cliente> findByCpfOrNome(String cpf, String nome, Pageable pageable);
}
