package com.example.demo.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.time.LocalDate;
import java.time.Period;

import com.example.demo.controller.ClienteController;
import com.example.demo.dto.ClienteDto;
import com.example.demo.model.Cliente;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ClienteModelAssembler extends RepresentationModelAssemblerSupport<Cliente, ClienteDto> {
    public ClienteModelAssembler() {
        super(ClienteController.class, ClienteDto.class);
    }

    @Override
    public ClienteDto toModel(Cliente clienteModel) {
        ClienteDto clienteDto = ClienteDto.builder() //
                .id(clienteModel.getId()) //
                .cpf(clienteModel.getCpf()) //
                .nome(clienteModel.getNome()) //
                .dataNascimento(clienteModel.getDataNascimento()) //
                .idade(calculaIdade(clienteModel.getDataNascimento())) //
                .build();

        clienteDto.add(linkTo(methodOn(ClienteController.class).getById(clienteModel.getId())).withSelfRel());

        return clienteDto;
    }

    private Integer calculaIdade(LocalDate nascimento) {
        if (nascimento == null)
            return 0;
        LocalDate hoje = LocalDate.now();
        return Period.between(nascimento, hoje).getYears();
    }
}
